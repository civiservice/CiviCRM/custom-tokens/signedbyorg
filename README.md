# Signed by Org

This provides simple additional tokens for [Fuzion Tokens](https://civicrm.org/extensions/fuzion-tokens) inserting signature data for the standard organization.


## Installation
* Install [Fuzion Tokens](https://civicrm.org/extensions/fuzion-tokens) extension if not yet done
* Clone this repo into the subfolder *tokens* in your Custom PHP directory (or save them manually in this directory) – e.g. *uploads/civicrm/custom_php/tokens*
* Activate added tokens in *Administer | Communications | Endabled Tokens*

## Usage
Use <code>{signedbyorg.signature_text}</code> and <code>{signedbyorg.signature_html}</code> to print a contact's primary signature data in either plain text or HTML. 

Hint: If there is no corresponding CMS user to the default organization, the signature data cannot be edited in the default contact edit form - Search Kit + Form Builder can be of help here.

## License
The extension is licensed under [AGPL-3.0](LICENSE.txt).
