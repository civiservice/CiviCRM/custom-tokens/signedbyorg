<?php
/**
 * Sammlung von Token zum Einfügen von Signaturdaten 
 * der Standard Organisation.
 * 
 * Collection of tokens for inserting signature data 
 * from the standard organization.
 * 
 * Copyright (C) 2022  civiservice.de GmbH (info@civiservice.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// Declare tokens
function signedbyorg_civitoken_declare($token){
  return array(
    $token. '.signature_html' => 'E-Mail-Signatur (html) Standardorganisation',
    $token. '.signature_text' => 'E-Mail-Signatur (Text) Standardorganisation',
  );
}

function signedbyorg_civitoken_get($cid, &$value){

    // Get contact_id of standard organization 
    $domains = civicrm_api4('Domain', 'get', [
        'currentDomain' => TRUE,
        'select' => [
        'contact_id',
        ],
        'limit' => 1,
        'checkPermissions' => FALSE,
    ]);
    $org_contact_id = $domains[0]['contact_id'];

    // Get email signature (html) from primary email address
    $result = civicrm_api4('Email', 'get', [
        'select' => [
        'signature_text', 
        'signature_html',
        ],
        'where' => [
            ['id', '=', $org_contact_id],
            ['is_primary', '=', TRUE],
        ],
        'limit' => 1,
        'checkPermissions' => FALSE,
    ]);
  
    if(isset($result[0]['signature_html'])) {
      $signature_html = $result[0]['signature_html'];
    } else $signature_html = "";
    
    if(isset($result[0]['signature_text'])) {
      $signature_text = $result[0]['signature_text'];
    } else $signature_text = "";  

    // Fill in values to tokens
    $value['signedbyorg.signature_html'] = $signature_html;
    $value['signedbyorg.signature_text'] = $signature_text;
}    